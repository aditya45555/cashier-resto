/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static classes.Api.isJSONValid;
import lib.Chilkat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author digitama1
 */
public class Order {

    Chilkat c = new Chilkat();

    public String getListOrder(String notable, String userid, String branchid) {
        String datajson = null;

        try {
            JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
            JSONArray params = new JSONArray(), data = new JSONArray();
            String json;
            int error;
            jo.put("no_meja", 0);

            if (notable != null) {
                jo.put("no_meja", notable);
            }

            String dataenc = c.encrypt(jo.toString(), branchid);
            jobj.put("user_id", userid);
            jobj.put("data", dataenc);

            json = c.decrypt(branchid, Chilkat.SendHttp(jobj.toString(), "api/cashier/listorder"));

            if (isJSONValid(json)) {
                jo = new JSONObject(json);
                params = jo.getJSONArray("result");
                datajson = String.valueOf(params);
                return datajson;
            }

        } catch (JSONException e) {
        }

        return datajson;
    }

}
