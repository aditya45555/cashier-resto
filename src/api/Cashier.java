/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static classes.Api.isJSONValid;
import lib.Chilkat;
import java.util.ArrayList;
import java.util.HashMap;
import lib.Helper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author digitama1
 */
public class Cashier {

    Chilkat c = new Chilkat();
    Helper h = new Helper();

    public ArrayList<HashMap<String, String>> getListCashier() {
        ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
            JSONArray params = new JSONArray(), data = new JSONArray();
            String json = null;
            int error;
            classes.Api ca = new classes.Api();
            boolean validjson = ca.isJSONValid(json);
            json = Chilkat.SendHttp(null, "api/data/getUserList");

            if (validjson = true) {
                jo = new JSONObject(json);
                data = jo.getJSONArray("result");
                error = 0;
                try {
                    error = jo.getInt("error");
                } catch (JSONException e) {
                    error = 0;
                }
                if (error == 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject d = data.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("nama", d.getString("name"));
                        map.put("email", d.getString("email"));
                        tempData.add(map);
                    }
                    return tempData;
                }

                error = 0;

            }
        } catch (JSONException e) {
        }
        return null;
    }

    public String loginCashier(String email, String password) {
        String datajson = null;
        try {
            JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
            JSONArray params = new JSONArray(), data = new JSONArray();
            String json;
            int error;

            jobj.put("email", email);
            String emailmd5 = h.md5(email);
            String encpassword = c.encrypt(password, emailmd5);

            jobj.put("password", encpassword);
            json = Chilkat.SendHttp(jobj.toString(), "api/account/login");
            if (isJSONValid(json)) {

                jo = new JSONObject(json);
                jo = jo.getJSONObject("result");

                datajson = String.valueOf(jo);

                return datajson;

            }
        } catch (JSONException e) {

        }
        return datajson;
    }

}
