/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import lib.Chilkat;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author digitama1
 */
public class Transaction {

    public String sendTransaction() {
        String json = null;
        try {
            JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
            JSONArray params = new JSONArray(), data = new JSONArray();
            int error;
            jobj.put("a", "a");
            jobj.put("id", 2);
            jobj.put("u", 23);
            json = Chilkat.SendHttp(jobj.toString(), "api/data/gettransaction");
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        }
        return json;
    }
}
