/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import form.formlogin;
import javax.swing.JFrame;
import lib.Helper;

/**
 *
 * @author digitama1
 */
public class MainClass {

    public static void loadchilkatlib() {
        try {
            System.load(new Helper().dirUser() + "/libaries/chilkat/libchilkat.so");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }

    public void showForm(JFrame frame) {
        frame.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        loadchilkatlib();
        form.formlogin fl = new formlogin();
        fl.setVisible(true);

    }

}
