/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import javax.swing.JOptionPane;
import lib.Helper;
import org.json.JSONObject;

/**
 *
 * @author digitama1
 */
public class Db {

    Connection con = null;

    private String readFileDB() {
        String dbjson = "";
        try {
            String filename = new Helper().dirUser() + "/config/db.json";
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                dbjson += scanner.nextLine();
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return dbjson;
    }

    public Connection konek() {
        try {
            JSONObject jo = new JSONObject(readFileDB());
            String host = jo.getString("host");
            String db = jo.getString("db");
            String username = jo.getString("username");
            String password = jo.getString("password");
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + host + "/" + db;
            con = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
            System.exit(0);
        }

        return con;
    }

}
