package classes;

import lib.Chilkat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api == null) {
            api = new Api();
        }

        return api;
    }

    public static ArrayList<HashMap<String, String>> getFeaturedPlaylist() {
        ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
            JSONArray params = new JSONArray(), data = new JSONArray();
            String json;
            int error;

            param.put("cookie", "");
            param.put("appid", "");
            param.put("userid", "");

            params.put(param);

            jobj.put("method", "getPlaylistByFeatured");
            jobj.put("id", 2);
            jobj.put("params", params);

            json = Chilkat.SendHttp(jobj.toString(), "playlist/getbyfeatured");
            if (isJSONValid(json)) {
                jo = new JSONObject(json);
                data = jo.getJSONArray("result");
                error = 0;
                try {
                    error = jo.getInt("error");
                } catch (JSONException e) {
                    error = 0;
                }
                if (error == 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject d = data.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("id", d.getString("id"));
                        map.put("description", d.getString("description"));
                        map.put("name", d.getString("name"));

                        tempData.add(map);
                    }
                    return tempData;
                }
            }
        } catch (JSONException e) {

        }
        return null;
    }

    public static boolean isJSONValid(String test) {
        if (test != null) {
            try {
                new JSONObject(test);
            } catch (JSONException ex) {
                // edited, to include @Arthur's comment
                // e.g. in case JSONArray is valid as well...
                try {
                    new JSONArray(test);
                } catch (JSONException ex1) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    

}
