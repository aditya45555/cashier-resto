/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author digitama1
 */
public class Helper {

    public String dirUser() {
        String diruser = System.getProperty("user.dir");
        return diruser;
    }

    public void createFileTransaction() {
        try {
            JSONObject jo = new JSONObject();
            jo.put("name", "a");
            jo.put("class", "a");
            jo.put("date", "a");
            File f = new File(dirUser() + "/temp/transaction.json");
            FileWriter file = new FileWriter(f);
            file.write(jo.toString());
            file.flush();
            file.close();
        } catch (JSONException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        } catch (IOException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public long strtotime(String d) {
        Date date;
        long time = 0;
        try {
            DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            if (d == null) {
                date = new Date();
            } else {
                date = dateformat.parse(d);
            }

            date = dateformat.parse(String.valueOf(dateformat.format(date)));
            time = date.getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        }
        return time;
    }

    public void deleteFileTransaction() {
        try {
            String dirtemp = dirUser() + "/temp/";
            File f = new File(dirtemp);
            File delfile;
            for (final File fileEntry : f.listFiles()) {
                delfile = new File(dirtemp + fileEntry.getName());
                delfile.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void centerComponent(JLabel label) {
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setVerticalAlignment(JLabel.CENTER);
    }

    public void maxSizeForm(JFrame frame) {
        Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        frame.setSize(maxBounds.width, maxBounds.height);
    }

    public String md5(String input) {
        if (input == null) {
            return null;
        }
        String passwordToHash = input;
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(passwordToHash.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
    
    
}
