package lib;

import com.chilkatsoft.CkCrypt2;
import com.chilkatsoft.CkHttp;
import com.chilkatsoft.CkHttpResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;

public class Chilkat {

    private static CkHttp shttp = null;
    private static CkHttpResponse resp;

    private static String readFileHttp() {
        String httpjson = "";
        try {
            String filename = new Helper().dirUser() + "/config/http.json";
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                httpjson += scanner.nextLine();
            }
            scanner.close();
            JSONObject jo = new JSONObject(httpjson);
            httpjson = jo.getString("http");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return httpjson;
    }

    private static String getUrl(String relativeUrl) {
        return  readFileHttp()+ relativeUrl;
    }

    public static CkCrypt2 getCkCrypt() {
        CkCrypt2 crypt = new CkCrypt2();
        boolean success;
        success = crypt.UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
        if (success != true) {
            return null;
        }

        crypt.put_EncodingMode("hex");
        crypt.put_HashAlgorithm("md5");
        crypt.put_CryptAlgorithm("aes");
        crypt.put_KeyLength(128);
        crypt.put_CipherMode("cbc");

        String iv, key;
        //key = crypt.hashStringENC(Api.authorizedid);
        //iv = crypt.hashStringENC(key);
        //crypt.SetEncodedKey(key,"hex");
        //crypt.SetEncodedIV(iv,"hex");
        return crypt;
    }

    public String encrypt(String val,String key) {
        String encryptBranch = null;
        CkCrypt2 crypt = new CkCrypt2();
        Boolean success = crypt.UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
        if (success != true) {
            return null;
        }
        crypt.put_CryptAlgorithm("aes");
        crypt.put_CipherMode("cbc");
        crypt.put_KeyLength(256);
        crypt.SetEncodedKey(key, "hex");
        crypt.SetEncodedIV("000102030405060708090A0B0C0D0E0F", "hex");
        encryptBranch = crypt.encryptStringENC(val);
        
        return encryptBranch;
    }
    
    

    public String decrypt(String key, String val) {
        String encryptBranch = null;
        CkCrypt2 crypt = new CkCrypt2();
        Boolean success = crypt.UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
        if (success != true) {
            return null;
        }

        crypt.put_CryptAlgorithm("aes");
        crypt.put_CipherMode("cbc");
        crypt.put_KeyLength(256);
        crypt.SetEncodedKey(key, "hex");
        crypt.SetEncodedIV("000102030405060708090A0B0C0D0E0F", "hex");
        encryptBranch = crypt.decryptStringENC(val);
        return encryptBranch;
    }

    private static void sHttpInit() {
        shttp = new CkHttp();
        shttp.UnlockComponent("WILLAWHttp_QxVksXRMURBF");
        shttp.SetRequestHeader("Content-type", "application/json");
        shttp.put_AcceptCharset("");
        shttp.put_UserAgent("");
        shttp.put_AcceptLanguage("");
        shttp.put_AllowGzip(false);

        shttp.put_CookieDir("memory");
        shttp.put_SaveCookies(true);
        shttp.put_SendCookies(true);

    }

    public static String SendHttpGet(String json, String url) {
        if (shttp == null) {
            sHttpInit();
        }

        resp = shttp.QuickGetObj(getUrl(url));
        if (resp == null) {
            return null;
        }

        return resp.bodyStr();
    }

    public static String SendHttp(String json, String url) {
        if (shttp == null) {
            sHttpInit();
        }

        resp = shttp.PostJson(getUrl(url), json);
        if (resp == null) {
            return null;
        }

        return resp.bodyStr();
    }

}
